﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchivementManager : MonoBehaviour {
	public static AchivementManager instance;

	void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void Login(){
		Social.localUser.Authenticate ((bool success)=> {});
	}
	public void ShowAchivement(){
		if (Social.localUser.authenticated) {
			Social.ShowAchievementsUI ();
		} else {
			Login ();
		}
	}
	public void checkAchivement(){
		if (ScoreManager.instance.score > 50) {
			Social.ReportProgress (Achivements.achievement_starter, 100f, (bool success) => {
			});
		}
		if (ScoreManager.instance.score > 200) {
			Social.ReportProgress (Achivements.achievement_mid_level, 100f, (bool success) => {
			});
		}
		if (ScoreManager.instance.score > 500) {
			Social.ReportProgress (Achivements.achievement_upper_level, 100f, (bool success) => {
			});
		}
		if (ScoreManager.instance.score > 700) {
			Social.ReportProgress (Achivements.achievement_pro_level, 100f, (bool success) => {
			});
		}
		if (ScoreManager.instance.score > 1000) {
			Social.ReportProgress (Achivements.achievement_legend, 100f, (bool success) => {
			});
		}
	}
}
