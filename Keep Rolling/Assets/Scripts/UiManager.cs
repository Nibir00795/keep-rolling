﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UiManager : MonoBehaviour {
    public static UiManager instance;
    public GameObject KeepRollingPanel;
	public GameObject LeaderBoardBtn;
	public GameObject AchivementBtn;
    public GameObject gameOverPanel;
    public GameObject taptext;
    public Text score;
    public Text highscore1;
    public Text highscore2;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }



	// Use this for initialization
	void Start () {
        highscore1.text = "High Score: " + PlayerPrefs.GetInt("highScore");

    }
    public void GameStart()
    {
        
        taptext.SetActive(false);
		LeaderBoardBtn.SetActive (false);
		AchivementBtn.SetActive (false);
        KeepRollingPanel.GetComponent<Animator>().Play("panel");
    }
	public void GameOver()
    {
        score.text = PlayerPrefs.GetInt("score").ToString();
        highscore2.text = PlayerPrefs.GetInt("highScore").ToString();
        gameOverPanel.SetActive(true);
    }
    public void Reset()
    {
        SceneManager.LoadScene(0);
    }
	public void ShowLeaderBoard(){
		LeaderBoardManager.instance.ShowLeaderBoard ();
	}
	public void ShowAchivement(){
		AchivementManager.instance.ShowAchivement ();
	}
	// Update is called once per frame
	void Update () {
	
	}
}
